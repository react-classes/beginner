import React from "react";
import "./styles.css";

const Section = (props) => {
  const {
    id,
    title = "Titulo",
    paragraph = "lorep isupm ",
    image = null,
  } = props;
  return (
    <section id={id} className="section">
      <div className="section__container">
        <div className="section__container_img">
          <img src={image} />
        </div>
        <div className="section__container_content">
          <h2>{title}</h2>
          <div className="paragraph">
            <p>{paragraph}</p>
            <div className="paragraph_columns">
              <div className="column">1</div>
              <div className="column">2</div>
              <div className="column">3</div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
const Home = () => {
  return (
    <div>
      <div className="header"></div>
      <div className="menu"></div>

      <div className="container">
        <Section
          id="section-1"
          title="Titulo  1"
          paragraph="SADADAS DAD AD AD AD ADAS DASD ASD ASDAS DA"
        />
        <Section />
        <Section />
        <Section />
      </div>
    </div>
  );
};

export default Home;
